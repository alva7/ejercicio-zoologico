package Programa;

import Animales.Pajaros;
import Animales.Peces;
import Animales.Perros;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Perros perro1 = new Perros("Tuti", 6, 20);
		Peces pez1 = new Peces("Oscar",2,3);
		Pajaros paj1 = new Pajaros("Dei",4,5);
		
		perro1.comer(20);
		pez1.comer(10);
		paj1.comer(5);
		
		System.out.println("El peso de "+perro1.getNombre()+" es "+perro1.getPeso() + " kg.");
		System.out.println("El peso de " + pez1.getNombre()+ " es "+ pez1.getPeso() + " kg.");
		System.out.println("El peso de "+paj1.getNombre()+" es "+ paj1.getPeso()+ " kg.");
		
		perro1.ladrar();
		pez1.nadar();
		paj1.volar();
		
		System.out.println("La nueva edad del perro es: "+perro1.getEdad());
		System.out.println("La nueva edad del pez es: "+pez1.getEdad());
		System.out.println("La nueva edad del pajaro es: "+paj1.getEdad());
		
	}

}
